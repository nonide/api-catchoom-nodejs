const axios      = require('axios')
const FormData   = require('form-data')
const fs         = require('fs')

const PARAMETERS = require('./parameters.json')

function getUrl(name) {
    return `${PARAMETERS.RECOGNITION_HOSTNAME}/${PARAMETERS.RECOGNITION_API_VERSION}/${name}`
}

async function search(token, image) {
    const url = getUrl('search')

    let data = new FormData()
    data.append('token', token)
    data.append('version', PARAMETERS.SDK_VERSION)
    data.append('image', fs.createReadStream(image))

    const headers = Object.assign(data.getHeaders(), {
        'User-Agent': PARAMETERS.USER_AGENT,
    })
    
    return await axios({
        url,
        method: 'POST',
        headers,
        data
    })
}

async function timeStamp(token) {
    const url = getUrl('timestamp')
    const params = {
        token
    }
    return await axios({
        url,
        method: 'POST',
        params
    })
}

module.exports.search    = search
module.exports.timeStamp = timeStamp